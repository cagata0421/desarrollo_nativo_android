package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.io.SerializablePermission;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

      //  textView.setMovementMethod(LinkMovementMethod.getInstance());
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
       /* LayoutInflater inflater = LayoutInflater.from(this);
        RelativeLayout vp = (RelativeLayout)inflater.inflate(R.layout.activity_results_row, null);
        TextView title = (TextView)vp.findViewById(R.id.nombre);
       // title.setText((String) object.get("name"));
        title.setMovementMethod(LinkMovementMethod.getInstance());*/
        setContentView(R.layout.activity_detail);

        //Receptor del Objeto para la vista Pregunta 3.1

        String object_id = getIntent().getStringExtra("Pasa_codigo");

        //el metodo DONE se genera con el autocomplete de get callback

        DataQuery query = DataQuery.get("item"); query.getInBackground(object_id, new GetCallback<DataObject>() {
                    @Override
                    public void done(DataObject object, DataException e) {
                        //Llamado para acceder a los objetos por el ID (byId)
                        TextView title = (TextView)findViewById(R.id.nombre);
                         title.setText((String) object.get("name"));

                        TextView title2 = (TextView)findViewById(R.id.precio);
                        title2.setText((String) object.get("price")+"\u0024");

                        ImageView thumbnail=  (ImageView)findViewById(R.id.thumbnail);
                        thumbnail.setImageBitmap((Bitmap) object.get("image"));

                        TextView title3 = (TextView)findViewById(R.id.descripcion);
                        title3.setText((String) object.get("description"));
                    }
                });

                // FIN - CODE6

    }

}
